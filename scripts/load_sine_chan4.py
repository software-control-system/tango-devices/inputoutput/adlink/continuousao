from PyTango import*
import sys
import time
import math


try:
	#TODO: put the desired device proxy	
	contAO = DeviceProxy("generic/acq/ContinuousAO")
	
	###############load a sine signal on channel 4####################
	
	#TODO: put the correct BufferDepth (same as in the property of the device)
	BufferDepth = 1000
	
	#TODO: put the desired voltage in volts (verify the the input range is coherent with this value)
	# The input range is gave by properties Polarity, AORefA, AORefB, AORefSource
	max_voltage = 5.0 

	i = 1
	ao_data = [math.sin(0 * 2. * 3.14159 / BufferDepth) * max_voltage]
	

	while i<BufferDepth:
		ao_data[i:i] = [math.sin(i * 2. * 3.14159 / BufferDepth) * max_voltage]
		i = i + 1 

	print ao_data

	#put ao_data in the argin of the command contAO with the channel number
	argin =(ao_data,["4"])
	
	#load the waveform on the device
	contAO.SetAOScaledData(argin)
	
except:
	exctype , value = sys.exc_info()[:2]
	print "Failed with exception ", exctype
	for err in value :
		print "---ERROR ELEMENT-------"
		print "reason:" , err["reason"]
		print "description:" , err["desc"]
		print "origin:" , err["origin"]
		print "severity:" , err["severity"]
