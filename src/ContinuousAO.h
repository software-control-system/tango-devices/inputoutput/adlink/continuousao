//=============================================================================
//
// file :	 ContinuousAO.h
//
// description : Include for the ContinuousAO class.
//
// project :	Continuous Anolog Output
//
// $Author: vince_soleil $
//
// $Revision: 1.4 $
//
// $Log: not supported by cvs2svn $
// Revision 1.3  2005/06/15 10:22:17  abeilleg
// added attributes channelXEnable.
//
// Revision 1.2  2005/01/20 13:21:51  abeilleg
// following ASL modifications (states management).
//
// Revision 1.1.1.1  2004/12/06 10:20:44  syldup
// initial import
//
//
// copyleft :	  Synchrotron SOLEIL
//		  L'Orme des Merisiers
//		  Saint-Aubin - BP 48

//
//=============================================================================
//
//		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//	   (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _CONTINUOUSAO_H
#define _CONTINUOUSAO_H

#include <tango.h>
#include <ADLinkContinuousAO.h>


//using namespace Tango;

/**
 * @author	$Author: vince_soleil $
 * @version	$Revision: 1.4 $ $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace ContinuousAO
{

/**
 * Class Description:
 * Perform continuous anolog ouput (waveform generation) on the selected outputs.
 */

/*
 *	Device States Description:
 *	Tango::UNKNOWN :	
 *	Tango::STANDBY :	
 *	Tango::RUNNING :	
 *	Tango::FAULT :	
 */


class ContinuousAO: public Tango::Device_2Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//------------------------------------------------------------- 
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevDouble	*attr_channel0_read;
		Tango::DevDouble	*attr_channel1_read;
		Tango::DevDouble	*attr_channel2_read;
		Tango::DevDouble	*attr_channel3_read;
		Tango::DevDouble	*attr_channel4_read;
		Tango::DevDouble	*attr_channel5_read;
		Tango::DevDouble	*attr_channel6_read;
		Tango::DevDouble	*attr_channel7_read;
		Tango::DevDouble	*attr_errorCounter_read;
		Tango::DevShort	*attr_useBoardFifo_read;
		Tango::DevShort	*attr_channel0Enable_read;
		Tango::DevShort	*attr_channel1Enable_read;
		Tango::DevShort	*attr_channel2Enable_read;
		Tango::DevShort	*attr_channel3Enable_read;
		Tango::DevShort	*attr_channel4Enable_read;
		Tango::DevShort	*attr_channel5Enable_read;
		Tango::DevShort	*attr_channel6Enable_read;
		Tango::DevShort	*attr_channel7Enable_read;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	The number of the board in the chassis cPCI.
 */
	Tango::DevUShort	boardNum;
/**
 *	The output frequency (the rate the samples are outputed).
 */
	Tango::DevDouble	frequency;
/**
 *	1 if using a start trigger, 0 otherwise.
 */
	Tango::DevShort	startTrigger;
/**
 *	1 if using a stop trigger, 0 otherwise.
 */
	Tango::DevShort	stopTrigger;
/**
 *	The start trigger source. Analog trigger DTRIG or ATRIG.
 */
	Tango::DevUShort	startTriggerSource;
/**
 *	The stop trigger source. anolog trigger ATRIG or pin AFI0 of AFI1.
 */
	Tango::DevUShort	stopTriggerSource;
/**
 *	select the type of analog trigger. Can be BELOW (trigger occurs below ATRIGLevel)
 *	or can be ABOVE (trigger occurs above ATRIGLevel).
 */
	Tango::DevUShort	aTRIGSelection;
/**
 *	The analog trigger level in volts.
 */
	Tango::DevDouble	aTRIGLevel;
/**
 *	The edges on which the trigger are detected. Can be RISING or FALLING.
 */
	Tango::DevUShort	dTRIGPolarity;
/**
 *	The value of the reference voltage gave on pin AORefA.
 */
	Tango::DevDouble	aORefA;
/**
 *	The value of the reference voltage gave on pin AORefB.
 */
	Tango::DevDouble	aORefB;
/**
 *	BP for unipolar outputs or UP for bipolar outputs.
 */
	Tango::DevUShort	polarity;
/**
 *	Select the internal reference (10.0 volts) with INTERN or the external references
 *	AORefA and AORefB with EXTERN.
 */
	Tango::DevUShort	aORefSource;
/**
 *	1 if enable channel 0, 0 otherwise.
 */
	Tango::DevShort	channel0Enable;
/**
 *	1 if enable channel 1, 0 otherwise.
 */
	Tango::DevShort	channel1Enable;
/**
 *	1 if enable channel 2, 0 otherwise.
 */
	Tango::DevShort	channel2Enable;
/**
 *	1 if enable channel 3, 0 otherwise.
 */
	Tango::DevShort	channel3Enable;
/**
 *	1 if enable channel 4, 0 otherwise.
 */
	Tango::DevShort	channel4Enable;
/**
 *	1 if enable channel 5, 0 otherwise.
 */
	Tango::DevShort	channel5Enable;
/**
 *	1 if enable channel 6, 0 otherwise.
 */
	Tango::DevShort	channel6Enable;
/**
 *	1 if enable channel 7, 0 otherwise.
 */
	Tango::DevShort	channel7Enable;
/**
 *	The buffer depth for <b>one channel</b>.
 */
	Tango::DevUShort	bufferDepth;
/**
 *	The waveform for channel 0 (attribute persistency).
 */
	vector<double>	channel0Waveform;
/**
 *	The waveform for channel 1 (attribute persistency).
 */
	vector<double>	channel1Waveform;
/**
 *	The waveform for channel 2 (attribute persistency).
 */
	vector<double>	channel2Waveform;
/**
 *	The waveform for channel 3 (attribute persistency).
 */
	vector<double>	channel3Waveform;
/**
 *	The waveform for channel 4 (attribute persistency).
 */
	vector<double>	channel4Waveform;
/**
 *	The waveform for channel 5 (attribute persistency).
 */
	vector<double>	channel5Waveform;
/**
 *	The waveform for channel 6 (attribute persistency).
 */
	vector<double>	channel6Waveform;
/**
 *	The waveform for channel 7 (attribute persistency).
 */
	vector<double>	channel7Waveform;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s	Device Name
 */
	ContinuousAO(Tango::DeviceClass *,string &);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s	Device Name
 */
	ContinuousAO(Tango::DeviceClass *,const char *);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s	Device name
 *	@param d	Device description.
 */
	ContinuousAO(Tango::DeviceClass *,const char *,const char *);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~ContinuousAO() { delete_device(); };
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name ContinuousAO methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values from hardware acquisition result.
 */
	virtual void read_attr(Tango::Attribute &attr);
/**
 * start the generation.
 *	@exception DevFailed
 */
	void	start();
/**
 * stop the generation.
 *	@exception DevFailed
 */
	void	stop();
/**
 * Give one period of the signal to generated of a specified channel in volts.
 *	@param	argin	The channel number ,in string).  The output data in volts ,in double)
 *	@exception DevFailed
 */
	void	set_aoscaled_data(const Tango::DevVarDoubleStringArray *);

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//------------------------------------------------------------- 
   ADLinkContinuousAO* ao;
  // asl::ContinuousAOConfig* config;
   void set_internal_state(void);
   void on_fault(std::string p_error_message);
   // save a channel waveform in the database 
   void ContinuousAO::save_waveform(double* waveform, string wfm_channel);
   double* ch0;
   double* ch1;
   double* ch2;
   double* ch3;
   double* ch4;
   double* ch5;
   double* ch6;
   double* ch7;
   double err_ctr;
   short use_fifo;
   std::string m_error_message;

protected :	
	//	Add your own data members here
	//-----------------------------------------
};

}	// namespace

#endif	// _CONTINUOUSAO_H
