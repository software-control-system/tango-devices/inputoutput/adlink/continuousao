static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/InputOutput/ADLINK/ContinuousAO/src/ClassFactory.cpp,v 1.1.1.1 2004-12-06 10:20:44 syldup Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: syldup $
//
// $Revision: 1.1.1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <ContinuousAOClass.h>

/**
 *	Create ContinuousAOClass singleton and store it in DServer object.
 *
 * @author	$Author: syldup $
 * @version	$Revision: 1.1.1.1 $ $
 */

void Tango::DServer::class_factory()
{

	add_class(ContinuousAO::ContinuousAOClass::init("ContinuousAO"));

}
